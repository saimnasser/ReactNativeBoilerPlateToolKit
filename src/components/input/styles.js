import { StyleSheet } from 'react-native';
import AppColors from '~utills/AppColors';
import { height, width } from '~utills/Dimension';
import Fonts from '~utills/Fonts';

const styles = StyleSheet.create({

  label: {
    color: AppColors.grey_placeholder,
    marginBottom: height(1),
    fontFamily: Fonts.RobotoMedium,
    fontSize: width(3)
  },
  icon: {
    flex: 1,
    borderTopLeftRadius: width(10),
    borderBottomLeftRadius: width(10),
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconInputMask: (error) => ({
    flexDirection: 'row',
    width: width(85),
    backgroundColor: AppColors.white,
    borderRadius: width(10),
    borderWidth: 1,
    borderColor: error ? AppColors.red : AppColors.grey80,
    paddingVertical: height(0.5)
  }),
  input: {
    fontFamily: Fonts.RobotoLight,
    fontSize: width(3.3),
    color: AppColors.black,
    width: width(71),
    height: height(5),
    borderTopRightRadius: width(10),
    borderBottomRightRadius: width(10),
  },
  errorText: {
    color: AppColors.red,
    fontFamily: Fonts.RobotoLight,
    fontSize: width(2.7),
    marginLeft: width(3),
    marginTop: height(0.5)
  }

});
export default styles;
