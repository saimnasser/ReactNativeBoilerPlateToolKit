import React, { forwardRef, useImperativeHandle, useRef, useState } from 'react';
import { ActivityIndicator, Text, TextInput, TouchableOpacity, View } from 'react-native';
import AppColors from '~utills/AppColors';
import { ProfileSvg } from '~utills/Icons';
import styles from './styles';

const Input = forwardRef(({
  containerStyle = {},
  inputStyle = {},
  label = '? ? ? ?',
  placeholder,
  placeholderTextColor = AppColors.grey_placeholder,
  maxLength,
  secureTextEntry,
  onSubmitEditing,
  keyboardType,
  blurOnSubmit = false,
  onChangeText = () => { }
}, ref) => {
  const [text, setText] = useState('')
  const [error, setError] = useState(null)
  const inputRef = useRef(null)

  useImperativeHandle(ref, () => ({
    getText: () => { return text },
    setError: (error) => setError(error),
    focus: () => inputRef.current.focus()
  }))

  return (
    <View style={containerStyle}>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.iconInputMask(error)}>
        <View style={styles.icon}>
          <ProfileSvg />
        </View>
        <TextInput
          ref={inputRef}
          value={text}
          onChangeText={val => {
            onChangeText()
            setText(val)
          }}
          style={[styles.input, inputStyle]}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          maxLength={maxLength}
          keyboardType={keyboardType}
          secureTextEntry={secureTextEntry}
          onSubmitEditing={onSubmitEditing}
          blurOnSubmit={blurOnSubmit}
        />
      </View>
      {error && <Text style={styles.errorText}>{error}</Text>}
    </View>
  );
});

export default Input;
