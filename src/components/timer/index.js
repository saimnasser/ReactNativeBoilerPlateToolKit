import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import CircularProgress from 'react-native-circular-progress-indicator';
import AppColors from '~utills/AppColors';
import { height, width } from '~utills/Dimension';
import styles from './styles';

const Timer = ({
    containerStyle = {},
    seconds = 60
}) => {
    const [timer, settimer] = useState(seconds);
    const timerFunc = () => {
        settimer(
            timer =>
            (timer =
                timer <= 0 ? '00' : timer - 1 < 10 ? `0${timer - 1}` : timer - 1),
        );
    };
    useEffect(() => {
        window.calltimer = setInterval(timerFunc, 1000);
    }, []);
    useEffect(() => {
        if (timer == 0) {
            clearInterval(window.calltimer);
        }
    }, [timer]);
    return (
        <View style={[styles.circle, containerStyle]}>
            <View style={styles.progress}>
                <CircularProgress
                    value={timer}
                    showProgressValue={false}
                    radius={height(7.5)}
                    progressValueColor={AppColors.beige}
                    inActiveStrokeColor={AppColors.transparent}
                    activeStrokeColor={AppColors.beige}
                    activeStrokeWidth={3}
                    maxValue={60}
                />
            </View>
            <View style={styles.dottedCircle} />
            <Text style={styles.timerText}>{timer}</Text>
        </View>
    );
};

export default Timer;
