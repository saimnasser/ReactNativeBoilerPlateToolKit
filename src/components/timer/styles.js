import { StyleSheet } from 'react-native';
import AppColors from '~utills/AppColors';
import { height, width } from '~utills/Dimension';
import Fonts from '~utills/Fonts';

const styles = StyleSheet.create({
    circle: {
        backgroundColor: AppColors.white,
        height: height(16),
        width: height(16),
        borderRadius: height(8),
        alignItems: 'center',
        justifyContent: 'center'
    },
    progress: {
        position: 'absolute',
        top: height(0.5),
        bottom: 0,
        left: width(1),
        right: 0,
        zIndex: 1
    },
    dottedCircle: {
        backgroundColor: AppColors.white,
        borderWidth: 1,
        borderColor: AppColors.beige,
        borderStyle: 'dashed',
        height: height(14),
        width: height(14),
        borderRadius: height(7)
    },
    timerText: {
        fontFamily: Fonts.RobotoMedium,
        color: AppColors.black,
        fontSize: width(5),
        position: 'absolute',
        alignItems: 'center'
    }
});
export default styles;
