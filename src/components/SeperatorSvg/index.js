import React from 'react';
import Svg, { Path } from "react-native-svg";

const SeperatorSvgComponent = (props) => {
  const {
    width,
    height,
    fill = 'none',
    stroke = '#98989880',
  } = props
  return (
    <Svg
      width={width}
      height={height}
      fill={fill}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 1h25.032l10.007 10 9.947-10H361"
        stroke={stroke}
        strokeLinecap="square"
      />
    </Svg>
  );
};

export default SeperatorSvgComponent;
