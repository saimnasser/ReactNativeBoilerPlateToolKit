import { createSlice } from '@reduxjs/toolkit';
import English from '~language/en'
import Russian from '~language/ru'

const initialState = {
    language: 'en',
    languageFile: English,
    // language: 'ru',
    // languageFile: Russian,
    isRTL: false
};
export const languageSlice = createSlice({
    name: 'Language',
    initialState,
    reducers: {
        setLanguage: (state, action) => {
            switch (action.payload) {
                case 'en':
                    state.language = 'en'
                    state.languageFile = English
                    break;
                case 'ru':
                    state.language = 'ru'
                    state.languageFile = Russian
                    break;
                default:
                    state.language = 'en'
                    state.languageFile = English
                    break;
            }

        },
    },
});

export const { login, logout, updateUser } = languageSlice.actions;
export const selectLanguageFile = state => state.Language.languageFile;
export default languageSlice.reducer;