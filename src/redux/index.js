import { configureStore } from '@reduxjs/toolkit';
import authReducer from './slices/authSlice';
import configReducer from './slices/configSlice';
import languageReducer from './slices/languageSlice';

export const store = configureStore({
    reducer: {
        Auth: authReducer,
        Config: configReducer,
        Language: languageReducer
    },
});