export const countryPhoneList = [
    { country: 'USA', code: '+ 1', val: +1 },
    { country: 'PK', code: '+ 92', val: +92 },
    { country: 'RU', code: '+ 7', val: +7 },
    { country: 'AFG', code: '+ 93', val: +93 },
    { country: 'KGZ', code: '+ 996', val: +996 },
]