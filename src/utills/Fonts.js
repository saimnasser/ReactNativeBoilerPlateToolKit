const Fonts = {
    RobotoBlack: 'Roboto-Black',
    RobotoBold: 'Roboto-Bold',
    RobotoMedium: 'Roboto-Medium',
    RobotoThin: 'Roboto-Thin',
    RobotoLight: 'Roboto-Light',
};
export default Fonts;