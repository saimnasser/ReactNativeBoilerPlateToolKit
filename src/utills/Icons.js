import BackArrowIcon from '~assets/icons/arrow_back.svg'
import SeperatorSvg from '~assets/icons/seperator.svg'
import LineSvg from '~assets/icons/line.svg'
import CrossSvg from '~assets/icons/cross.svg'
import LockSvg from '~assets/icons/lock.svg'
import ProfileSvg from '~assets/icons/profile.svg'

export { BackArrowIcon, SeperatorSvg, LineSvg, CrossSvg, LockSvg, ProfileSvg }