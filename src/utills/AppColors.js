const AppColors = {
  white: '#ffffff',
  black: '#231F20',
  red: 'red',
  transparent: 'transparent',
  beige: '#A18E7F',
  grey: '#80808080',
  grey_text: '#ADACAC',
  grey80: '#98989880',
  grey_placeholder: '#969696',
  bgGradient: ['#A18E7F20', '#A18E7F50', '#A18E7F99'],
  bgGradientAndroid: []
};

export default AppColors;