const ScreenNames = {
  LOGIN: 'LOGIN',
  HOME: 'Home',
  LANDING: 'Landing',
  GET_STARTED: 'GET_STARTED',
  VERIFY_PHONE_NUMBER: 'VERIFY_PHONE_NUMBER',
  ADD_PROFILE_INFO: 'ADD_PROFILE_INFO'
}
export default ScreenNames;