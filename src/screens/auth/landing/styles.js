import { Platform, StyleSheet } from 'react-native';
import { height, width } from '~utills/Dimension';
import Fonts from '~utills/Fonts';
import AppColors from '../../../utills/AppColors';

const styles = StyleSheet.create({
  mainViewContainer: {
    flex: 1,
  },
  title: {
    color: AppColors.black,
    fontWeight: 'bold',
    fontSize: width(4),
    marginBottom: height(2)
  },
  subtitle1: {
    fontFamily: Fonts.RobotoMedium,
    color: AppColors.white,
    position: 'absolute',
    bottom: height(26),
    marginHorizontal: width(5),
    alignSelf: 'center',
    fontSize: width(3.7)
  },
  subtitle2: {
    fontFamily: Fonts.RobotoMedium,
    color: AppColors.white,
    position: 'absolute',
    bottom: height(28.5),
    marginHorizontal: width(5),
    alignSelf: 'center',
    fontSize: width(3.7)
  },
  best: {
    color: AppColors.white,
    position: 'absolute',
    top: Platform.OS == 'ios' ? height(9.5) : height(8.7),
    left: width(8),
    fontSize: width(3)
  },
  bottomBtns: {
    position: 'absolute',
    bottom: height(8),
    alignSelf: 'center',
    height: height(15),
    justifyContent: 'space-around'
  },
  shi: {
    color: AppColors.white,
    fontSize: width(22),
    position: 'absolute',
    left: width(31),
    top: height(40)
  },
  on: {
    color: AppColors.white,
    fontSize: width(22),
    position: 'absolute',
    left: width(50),
    top: height(51)
  }
});
export default styles;
