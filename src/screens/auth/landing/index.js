import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { login } from '~redux/slices/authSlice';
import { Button, ScreenWrapper } from '~components';
import FashionBG from '~assets/images/BG.png'
import AppColors from '~utills/AppColors';
import ScreenNames from '~routes/routes';
import { selectLanguageFile } from '~redux/slices/languageSlice';
export default function Landing({ navigation, route }) {
    const dispatch = useDispatch()
    const languageFile = useSelector(selectLanguageFile)
    return (
        <ScreenWrapper
            transclucent
            backgroundImage={FashionBG}
            backgroundColor={AppColors.transparent}
            statusBarColor={AppColors.transparent}>

            <View style={styles.mainViewContainer}>
                <Text style={styles.best}>B   E   S   T</Text>
                <Text style={styles.subtitle1}>{languageFile.landingTitle2}</Text>
                <Text style={styles.subtitle2}>{languageFile.landingTitle1}</Text>
                <Text style={styles.shi}>SHI</Text>
                <Text style={styles.on}>ON</Text>
                <View style={styles.bottomBtns}>
                    <Button
                        title={languageFile.getStarted}
                        onPress={() => navigation.navigate(ScreenNames.GET_STARTED)} />
                    <Button
                        secondary
                        onPress={() => navigation.navigate(ScreenNames.LOGIN)}
                        title={languageFile.alreadyMember} />
                </View>
            </View>

        </ScreenWrapper>
    );
}
