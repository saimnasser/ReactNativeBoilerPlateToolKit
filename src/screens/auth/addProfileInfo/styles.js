import { Platform, StyleSheet } from 'react-native';
import { height, width } from '~utills/Dimension';
import AppColors from '../../../utills/AppColors';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Fonts from '~utills/Fonts';

const styles = StyleSheet.create({
  mainViewContainer: {
    flex: 1,
    marginTop: getStatusBarHeight(),
    // paddingHorizontal: width(6),
    paddingVertical: height(2)
  },
  header: {
    height: height(8),
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: width(6),
    paddingVertical: height(1.5),
    alignItems: 'center'
  },
  title: {
    fontFamily: Fonts.RobotoBold,
    color: AppColors.black,
    fontSize: width(5),
    marginBottom: height(1.5),
    alignSelf: 'center'
  },
  subtitle: {
    fontFamily: Fonts.RobotoMedium,
    color: AppColors.black,
    textAlign: 'center',
    fontSize: width(3.7),
    marginBottom: height(2)
  },
  textContainer: {
    marginTop: height(6),
    marginHorizontal: width(4)
  },
  inputContainer: {
    alignSelf: 'center',
    marginTop: height(2)
  },
  btn: {
    marginTop: height(4)
  },
  bottomText: {
    fontFamily: Fonts.RobotoMedium,
    alignSelf: 'center',
    color: AppColors.black,
    marginTop: height(20)
  },
});
export default styles;
