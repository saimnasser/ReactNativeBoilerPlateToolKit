import React, { useEffect, useRef } from 'react';
import { TouchableOpacity, View, Text } from "react-native";
import { useSelector } from 'react-redux';
import { Button, Input, ScreenWrapper } from '~components';
import { selectLanguageFile } from '~redux/slices/languageSlice';
import AppColors from '~utills/AppColors';
import { BackArrowIcon, CrossSvg } from '~utills/Icons';
import styles from './styles';

export default function AddProfileInfo({ navigation, route }) {
    const nameInputRef = useRef(null).current
    const passInputRef = useRef(null)
    const confirmInputRef = useRef(null)
    const languageFile = useSelector(selectLanguageFile)
    const onSignInPress = () => {
        const password = passInputRef.current.getText()
        const confirmPassword = confirmInputRef.current.getText()
        console.log(password)

        if (password != confirmPassword) {
            confirmInputRef.current.setError('Passwords dont match')
        }
    }
    return (
        <ScreenWrapper
            linearGradient
            scrollEnabled
            transclucent
            statusBarColor={AppColors.transparent}
            backgroundColor={AppColors.transparent}>
            <View style={styles.mainViewContainer}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backContainer}>
                        <BackArrowIcon />
                    </TouchableOpacity>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{languageFile.almostDoneC}</Text>
                    <Text style={styles.subtitle}>{languageFile.enterInfoPhrase}</Text>
                </View>
                <Input
                    ref={nameInputRef}
                    containerStyle={styles.inputContainer}
                    label={languageFile.Name}
                    placeholder={languageFile.egName}
                    onSubmitEditing={() => passInputRef.current.focus()}
                />
                <Input
                    ref={passInputRef}
                    containerStyle={styles.inputContainer}
                    label={languageFile.passwordLabel}
                    maxLength={4}
                    placeholder={'• • • •'}
                    secureTextEntry
                    keyboardType={'number-pad'}
                    onChangeText={() => {
                        passInputRef.current.setError(null)
                        confirmInputRef.current.setError(null)
                    }}
                    onSubmitEditing={() => confirmInputRef.current.focus()}
                />
                <Input
                    ref={confirmInputRef}
                    containerStyle={styles.inputContainer}
                    label={languageFile.confirmPassLabel}
                    placeholder={'• • • •'}
                    maxLength={4}
                    secureTextEntry
                    onChangeText={() => {
                        passInputRef.current.setError(null)
                        confirmInputRef.current.setError(null)
                    }}
                    keyboardType={'number-pad'}
                    blurOnSubmit
                />

                <Button
                    onPress={onSignInPress}
                    containerStyle={styles.btn}
                    title={languageFile.SignIn} />
                <Text style={styles.bottomText}>{languageFile.haveAccount}</Text>
            </View>
        </ScreenWrapper>
    );
}
