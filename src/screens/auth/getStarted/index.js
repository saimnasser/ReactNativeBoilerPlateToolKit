import React, { useState } from 'react';
import { Platform, Text, TextInput, TouchableOpacity, View } from "react-native";
import ModalDropdown from 'react-native-modal-dropdown';
import { useDispatch, useSelector } from 'react-redux';
import { Button, ScreenWrapper } from '~components';
import { selectLanguageFile } from '~redux/slices/languageSlice';
import ScreenNames from '~routes/routes';
import AppColors from '~utills/AppColors';
import { BackArrowIcon, SeperatorSvg } from '~utills/Icons';
import { countryPhoneList } from '~utills/StaticData';
import styles from './styles';
import { width } from '~utills/Dimension';
import SeperatorSvgComponent from '~components/SeperatorSvg';


export default function GetStarted({ navigation, route }) {
    const languageFile = useSelector(selectLanguageFile)
    const [phoneNum, setPhoneNum] = useState('')
    const [selectedCountry, setSelectedCountry] = useState(countryPhoneList[0])
    const [countryList, setCountry] = useState(countryPhoneList)

    const renderCountryRow = ({ country, code }) => {
        return (
            <View style={styles.countryRow}>
                <Text style={
                    country == selectedCountry?.country ?
                        styles.countryRowTextSelected
                        : styles.countryRowText
                }>
                    {country}
                </Text>
            </View >
        )
    }
    const onConfirmPress = () => navigation.navigate(ScreenNames.VERIFY_PHONE_NUMBER, { phoneNumber: selectedCountry?.val + phoneNum })


    return (
        <ScreenWrapper
            linearGradient
            scrollEnabled
            transclucent
            statusBarColor={AppColors.transparent}
            backgroundColor={AppColors.transparent}>
            <View style={styles.mainViewContainer}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backContainer}>
                        <BackArrowIcon />
                    </TouchableOpacity>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{languageFile.getStartedC}</Text>
                    <Text style={styles.subtitle}>{languageFile.confirmCodePhrase}</Text>
                </View>
                <ModalDropdown
                    animated
                    onSelect={(i, val) => setSelectedCountry(val)}
                    renderRowProps={styles.underLayColor}
                    dropdownStyle={styles.dropContainer}
                    renderRow={renderCountryRow}
                    options={countryList}>
                    <View style={styles.modalValueContainer}>
                        <View style={styles.line1} />
                        <Text style={styles.countryText}>{selectedCountry?.country}</Text>
                    </View>
                </ModalDropdown>
                <View style={styles.vectorContainer}>

                    <SeperatorSvgComponent height={12} width={Platform.OS == 'ios' ? width(92.5) : width(88)} />

                    <View style={styles.inputContainer}>
                        <View style={styles.leftBox}>
                            <Text style={styles.countryPhoneCode}>{selectedCountry?.code}</Text>
                        </View>
                        <View style={styles.rightBox}>
                            <TextInput
                                value={phoneNum}
                                onChangeText={text => setPhoneNum(text)}
                                keyboardType={'number-pad'}
                                placeholderTextColor={AppColors.grey}
                                placeholder={languageFile.yourPhoneNum}
                                style={styles.textInput} />
                        </View>
                    </View>
                </View>
                <View style={styles.line2} />
                <Button onPress={onConfirmPress} containerStyle={styles.btn} title={languageFile.Confirm} />
                <Text style={styles.bottomText}>{languageFile.haveAccount}</Text>
            </View>
        </ScreenWrapper>
    );
}
