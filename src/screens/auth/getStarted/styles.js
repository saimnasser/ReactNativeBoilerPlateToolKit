import { Platform, StyleSheet } from 'react-native';
import { height, width } from '~utills/Dimension';
import AppColors from '../../../utills/AppColors';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Fonts from '~utills/Fonts';

const styles = StyleSheet.create({
  mainViewContainer: {
    flex: 1,
    marginTop: getStatusBarHeight(),
    // paddingHorizontal: width(6),
    paddingVertical: height(2)
  },
  header: {
    height: height(8),
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: width(6),
    paddingVertical: height(1.5),
    alignItems: 'center'
  },
  title: {
    fontFamily: Fonts.RobotoBold,
    color: AppColors.black,
    fontSize: width(5),
    marginBottom: height(1.5),
    alignSelf: 'center'
  },
  subtitle: {
    fontFamily: Fonts.RobotoMedium,
    color: AppColors.black,
    textAlign: 'center',
    fontSize: width(3.7),
    marginBottom: height(2)
  },
  textContainer: {
    marginTop: height(8)
  },
  line1: {
    height: 1.25,
    width: Platform.OS == 'ios' ? width(92.5) : width(88),
    backgroundColor: AppColors.grey80,
    alignSelf: 'flex-end',

  },
  line2: {
    height: 1.25,
    width: Platform.OS == 'ios' ? width(92.5) : width(88),
    backgroundColor: AppColors.grey80,
    alignSelf: 'flex-end',
    marginTop: height(1),
    marginBottom: height(2)
  },
  verticalLine: {
    width: 1,
    height: '100%',
    backgroundColor: AppColors.grey,
    left: width(18)
    // marginBottom: -height(2)
  },
  bottomText: {
    fontFamily: Fonts.RobotoMedium,
    alignSelf: 'center',
    color: AppColors.black,
    marginTop: Platform.OS == 'ios' ? height(30) : height(33)
  },
  countryContainer: {
    height: height(7),
    justifyContent: 'center',
  },
  countryText: {
    color: AppColors.black,
    fontFamily: Fonts.RobotoMedium,
    marginTop: height(2),
    marginLeft: width(6),

  },
  inputContainer: {
    height: height(5),
    flexDirection: 'row',
  },
  leftBox: {
    height: '100%',
    width: width(20),
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: AppColors.grey80
  },
  rightBox: {
    height: '100%',
    flex: 1,
  },
  countryPhoneCode: {
    color: AppColors.black,
    fontFamily: Fonts.RobotoMedium,
    fontSize: width(3.5)
  },
  textInput: {
    flex: 1,
    paddingLeft: width(4),
    fontSize: width(3.7),
    color: AppColors.black
  },
  btn: {
    marginTop: height(4)
  },
  vectorContainer: { alignSelf: 'flex-end', },
  dropDown: {},
  dropContainer: {
    width: width(100),
    alignSelf: 'baseline',
    marginTop: Platform.OS == 'ios' ? 0 : -height(3.6),
  },
  modalValueContainer: {
    width: width(100),
    height: height(7),
    marginTop: height(4),
  },
  countryRow: {

  },
  countryRowText: {
    color: AppColors.black,
    paddingVertical: height(2),
    marginLeft: width(6),
    fontFamily: Fonts.RobotoMedium
  },
  countryRowTextSelected: {
    color: AppColors.beige,
    paddingVertical: height(2),
    marginLeft: width(6),
    fontFamily: Fonts.RobotoMedium

  },
  underLayColor: { underlayColor: AppColors.grey80 }

});
export default styles;
