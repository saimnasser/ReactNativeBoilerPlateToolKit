import { Platform, StyleSheet } from 'react-native';
import { height, width } from '~utills/Dimension';
import AppColors from '../../../utills/AppColors';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Fonts from '~utills/Fonts';

const styles = StyleSheet.create({
  mainViewContainer: {
    flex: 1,
    marginTop: getStatusBarHeight(),
    // paddingHorizontal: width(6),
    paddingVertical: height(2)
  },
  header: {
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: width(6),
    paddingVertical: height(1.5),
    alignItems: 'center'
  },
  backContainer: { flex: 1 },
  crossBtnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppColors.white,
    height: height(5),
    width: height(5),
    borderRadius: height(2.5),
  },
  title: {
    fontFamily: Fonts.RobotoBold,
    color: AppColors.black,
    fontSize: width(5),
    marginBottom: height(1.5),
    alignSelf: 'center'
  },
  subtitle: {
    fontFamily: Fonts.RobotoMedium,
    color: AppColors.black,
    textAlign: 'center',
    fontSize: width(3.7),
    marginHorizontal: width(5),
    marginBottom: height(2)
  },
  textContainer: {
    marginTop: height(8)
  },
  otpInput: {
    color: AppColors.black,
    backgroundColor: AppColors.white,
    height: height(8),
    width: height(8),
    borderRadius: height(4),
    borderWidth: 1,
    borderColor: AppColors.grey
  },
  otpContainer: {
    width: width(80),
    height: height(15),
    alignSelf: 'center'
  },
  verifyBtn: {
    marginTop: height(4)
  },
  footerText: {
    fontFamily: Fonts.RobotoMedium,
    alignSelf: 'center',
    color: AppColors.black,
    marginTop: height(2)
  },
  resendText: {
    textDecorationLine: 'underline',
    color: AppColors.beige
  },
  timer: {
    alignSelf: 'center',
    marginTop: height(18)
  }
});
export default styles;
