import React, { useEffect, useRef, useState } from 'react';
import { TouchableOpacity, View, Text } from "react-native";
import { Button, ScreenWrapper } from '~components';
import AppColors from '~utills/AppColors';
import { BackArrowIcon, CrossSvg } from '~utills/Icons';
import styles from './styles';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { height, width } from '~utills/Dimension';
import Timer from '~components/timer';
import ScreenNames from '~routes/routes';
import { selectLanguageFile } from '~redux/slices/languageSlice';
import { useSelector } from 'react-redux';

export default function VerifyPhoneNumber({ navigation, route }) {
    const { phoneNumber } = route.params
    const languageFile = useSelector(selectLanguageFile)
    const phoneRef = useRef(null)
    const [code, setCode] = useState('')
    useEffect(() => {
        setTimeout(() => {
            phoneRef.current.focusField(0)
        }, 600);
    }, [])
    return (
        <ScreenWrapper
            linearGradient
            scrollEnabled
            transclucent
            statusBarColor={AppColors.transparent}
            backgroundColor={AppColors.transparent}>
            <View style={styles.mainViewContainer}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backContainer}>
                        <BackArrowIcon />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setCode('')}
                        activeOpacity={0.7}
                        style={styles.crossBtnContainer}>
                        <CrossSvg />
                    </TouchableOpacity>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{languageFile.verificationC}</Text>
                    <Text style={styles.subtitle}>{languageFile.codeSentPhrase}</Text>
                </View>
                <OTPInputView
                    ref={phoneRef}
                    style={styles.otpContainer}
                    pinCount={4}
                    autoFocusOnLoad={false}
                    selectionColor={AppColors.grey}
                    codeInputFieldStyle={styles.otpInput}
                    codeInputHighlightStyle={styles.otpInput}
                    code={code}
                    onCodeChanged={code => setCode(code)}
                />
                <Button onPress={() => navigation.navigate(ScreenNames.ADD_PROFILE_INFO)} containerStyle={styles.verifyBtn} title={languageFile.Verify} />
                <Text style={styles.footerText}>{languageFile.didntGetCodePhrase} <Text style={styles.resendText}>{languageFile.Resend}</Text></Text>
                <Timer seconds={60} containerStyle={styles.timer} />
            </View>
        </ScreenWrapper>
    );
}
