import React from 'react';
import { Text, TouchableOpacity, View } from "react-native";
import { useSelector } from 'react-redux';
import { Button, Input, ScreenWrapper } from '~components';
import { selectLanguageFile } from '~redux/slices/languageSlice';
import AppColors from '~utills/AppColors';
import { BackArrowIcon } from '~utills/Icons';
import styles from './styles';

export default function Login({ navigation, route }) {
    const languageFile = useSelector(selectLanguageFile)



    return (
        <ScreenWrapper
            linearGradient
            scrollEnabled
            transclucent
            statusBarColor={AppColors.transparent}
            backgroundColor={AppColors.transparent}>
            <View style={styles.mainViewContainer}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backContainer}>
                        <BackArrowIcon />
                    </TouchableOpacity>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{languageFile.welcomeBackC}</Text>
                    <Text style={styles.subtitle}>{languageFile.loginPhrase}</Text>
                </View>
                <Input
                    maxLength={4}
                    secureTextEntry
                    label={languageFile.passwordLabel}
                    placeholder={'• • • •'}
                    containerStyle={styles.input}
                />
                <Button
                    title={languageFile.logIn} />
                <Button
                    secondary
                    title={languageFile.changePhoneNum}
                    containerStyle={styles.phoneNumBtn} />
                <Text style={styles.bottomText}>{languageFile.dontHaveAccount}</Text>

            </View>
        </ScreenWrapper>
    );
}
